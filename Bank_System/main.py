from tabnanny import check
from fastapi import FastAPI
from database import engine
from models import Base
from routers import users, accounts, login
from webapps.routers import user as web_user
from webapps.routers import auth as web_auth
from webapps.routers import homepage as web_homepage
from webapps.routers import accounts, check_balance

Base.metadata.create_all(bind = engine)

app = FastAPI()

# app.include_router(users.router)
# app.include_router(accounts.router)
# app.include_router(login.router)
app.include_router(web_user.router) 
app.include_router(web_auth.router)
app.include_router(web_homepage.router)
app.include_router(accounts.router)
app.include_router(check_balance.router)



