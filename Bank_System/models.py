from sqlalchemy import Column, Integer, String, Float, ForeignKey
from sqlalchemy.orm import relationship

from database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key = True, index=True)
    username = Column(String, unique = True,  nullable= False)
    account_no = Column(Integer, unique = True, nullable = False)
    password = Column(String, nullable = False)

    accounts = relationship("Account", back_populates = "users")

class Account(Base):
    __tablename__ = "accounts"
    id = Column(Integer,primary_key = True, index=True)
    account_no = Column(Integer, unique = True, nullable = False)
    curr_balance = Column(Float, nullable = False, default=1000)
    user_id =  Column(Integer, ForeignKey("users.id"))

    users = relationship("User",back_populates="accounts")