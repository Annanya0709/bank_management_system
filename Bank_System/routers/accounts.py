from http.client import HTTPException
from fastapi import APIRouter, Depends
from schemas import ShowAccount
from sqlalchemy.orm import Session
from database import get_db
from models import User, Account as AccountModel

router = APIRouter()

@router.get('/accounts/{username}', response_model = ShowAccount)
def get_account_details(username : str, db: Session=Depends(get_db)):
    user = db.query(User).all()
    account = db.query(AccountModel).filter(AccountModel.account_no==user.account_no).first()
    print(account)
    if not account:
        raise HTTPException(status_code = 404, detail = f" {username} does not exists")
    return account

@router.get('/accounts/all', response_model = ShowAccount)
def get_all_accounts(db: Session = Depends(get_db)):
    accounts = db.query(AccountModel).all()
    return accounts

# @router.post("/accounts")
# async def deposits(req: Request,
#                    email: str = Form(...),
#                    amount: str = Form(...),
#                    db: Session = Depends(get_db)):
#     form = await request.form()
#     email = form.get("email")
#     amount = form.get("amount")
#     print(email, amount)

#     user = db.query(models.users).filter(models.users.email == email).first()
#     print(user)
#     if user is None:
#         return "invalid user"
#     else:
#         user_deposit = db.query(models.deposits).filter(models.deposits.user_id == user.id).first()

#         if user_deposit:
#             print(user_deposit.amount, amount)
#             user_deposit.amount = int(user_deposit.amount) + int(amount)
#         else:
#             new_deposit = models.deposits(amount=amount,
#                                           user_id=user.id)
#             db.add(new_deposit)
#             # print(new_deposit)

#         db.commit()
#         db.close()
#         return templates.TemplateResponse("deposits.html", {"request": req})


# @router.post("/account/", response_model=ShowAccount)
# def insert_account_details(request : Request, account: Account, db: Session=Depends(get_db)):
#     user = db.query(User).filter(User.id==id).first()
#     name = user.name
#     accounts = AccountModel(name = name, curr_balance=1000)
#     db.add(accounts)
#     db.commit()
#     db.refresh(accounts)
#     return accounts
