from http.client import responses
from sqlite3 import IntegrityError
from sys import api_version
from fastapi import APIRouter, Request, Depends, responses, status,Response
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from models import User, Account as AccountModel
from database import get_db
from models import User

router = APIRouter(include_in_schema=False)

templates = Jinja2Templates(directory="templates")

@router.get('/home')
def user_details(request: Request):
    return templates.TemplateResponse('general_pages/homepage.html', {'request':request})


